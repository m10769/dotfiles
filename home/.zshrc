#   -----------------
#   Path locales list

PATH=/bin

path_locals=(

    # System

    "/usr/local/sbin"
    "/usr/local/bin"
    "/usr/bin"

    # Perl and Java

    "/usr/lib/jvm/default/bin"
    "/usr/bin/vendor_perl"
    "/usr/bin/site_perl"
    "/usr/bin/core_perl"

    # Home and custom paths

    "$HOME/.local/share/gem/ruby/3.0.0/bin"
    "$HOME/.local/scripts/bin"
    "$HOME/.local/app_images"
    "$HOME/.local/bin"
    "$HOME/.cargo/bin"
    "$HOME/bin"
)


# Append each PATH locale in PATH variable

for i in "${path_locals[@]}"; do
    PATH+=":$i"
done

export PATH


#   -------
#   Exports

export BAT_THEME="Nord"
export EDITOR="nvim"


#   ----------------------
#   Zsh config and theming

source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
source ~/.zsh/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh

[[ -s /home/kevin/.autojump/etc/profile.d/autojump.sh ]] && source /home/kevin/.autojump/etc/profile.d/autojump.sh
autoload -U compinit && compinit -u
setopt autocd


# Tab complete for ZSH

autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)   # Include hidden files.


# Vim keys

bindkey -e
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history


# Edit command in vim with ctrl-p

autoload edit-command-line; zle -N edit-command-line
bindkey '^p' edit-command-line


# Prompt theme

# eval "$(starship init zsh)"

[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.

if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

source ~/powerlevel10k/powerlevel10k.zsh-theme



#   -------------------------------
#   Aliasses to make my work easier

alias r="rm -r"
alias g="git"
alias n="nvim"
alias v="vim"
alias e="exit"
alias f="ranger"
alias t="tmux"

alias ll="exa -lam --no-user --time-style long-iso --group-directories-first -s extension --icons"
alias l="exa -lm --no-user --time-style long-iso --group-directories-first -s extension --icons"
alias tree="exa -lam --no-user --time-style long-iso --group-directories-first -s extension --icons --tree --git-ignore"
alias dust="dust -rx"
alias rg="rg -Usn"

alias cm="unimatrix -c blue -s 98 -a -b -o"
alias neofetch="neofetch --ascii ~/.config/neofetch/default"

alias fzf="fzf --border sharp --margin 10% --padding 5% --info inline --prompt 'SEARCH: ' --pointer '**' --ansi --color 'bg+:-1,pointer:green,fg+:green,hl:yellow,border:gray'"
# alias tree="tree --dirsfirst -F -a -I .git -I node_modules -I target"
alias grep="grep --colour=auto"

alias pyserver="python -m http.server 3000"
alias exe="chmod +x"

alias ,repo="cd ~/HDD/repos; ll"

alias ,projects="cd ~/HDD/projects; ll"
alias ,js="cd ~/HDD/projects/javascript; ll"
alias ,py="cd ~/HDD/projects/python; ll"
alias ,html="cd ~/HDD/projects/html; ll"
alias ,arduino="cd ~/HDD/projects/arduino; ll"

alias arduino-compile="arduino-cli compile -b arduino:avr:uno"
alias arduino-list="arduino-cli board list"
alias arduino-install="arduino-cli lib install"
alias arduino-uninstall="arduino-cli lib uninstall"
alias arduino-new="arduino-cli sketch new"

alias pip="python -m pip"
alias cdtmp="cd $(mktemp -d)"

alias minecraft="java -jar ~/Downloads/TLauncher-2.86/TLauncher-2.86.jar; exit"
[ -f /opt/miniconda3/etc/profile.d/conda.sh ] && source /opt/miniconda3/etc/profile.d/conda.sh

export PATH="$HOME/.poetry/bin:$PATH"
export PATH=/home/kevin/.cabal/bin:/home/kevin/.poetry/bin:/home/kevin/.autojump/bin:/bin:/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/lib/jvm/default/bin:/usr/bin/vendor_perl:/usr/bin/site_perl:/usr/bin/core_perl:/home/kevin/.local/share/gem/ruby/3.0.0/bin:/home/kevin/.local/scripts/bin:/home/kevin/.local/app_images:/home/kevin/.local/bin:/home/kevin/.cargo/bin:/home/kevin/bin
